﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DePDraw
{
    class UserShape : Command
    {
        public Rectangle shape;
        public bool mIsClick = false;
        public PictureBox mPictureBox;
        public bool paint = true;

        public CheckBox SelectModeCheck;
        public bool mMove = false;
        public int oldX;
        public int oldY;
        private int sizeNodeRect = 5;
        private Bitmap mBmp = null;
        public PosSizableRect nodeSelected = PosSizableRect.None;
        private bool selectMode;
        public bool selected = false;

        //Enum for positions of the resize rectangles
        public enum PosSizableRect
        {
            UpMiddle,
            LeftMiddle,
            LeftBottom,
            LeftUp,
            RightUp,
            RightMiddle,
            RightBottom,
            BottomMiddle,
            None

        };

        public UserShape()
        {
        }

        public UserShape(Rectangle r)
        {
            shape = r;
            mIsClick = false;
        }

        /// <summary>
        /// Empty draw method, children need own implementation
        /// </summary>
        public virtual void Draw(Graphics g)
        {
        }

        /// <summary>
        /// Draw the resize rectangles
        /// </summary>
        private void drawResizer(Graphics g)
        {
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            foreach (PosSizableRect pos in Enum.GetValues(typeof(PosSizableRect)))
            {
                Rectangle rec = GetRect(pos);
                g.DrawRectangle(new Pen(Color.Red), rec);
                g.FillRectangle(myBrush, rec);
            }
        }

        public void SetBitmapFile(string filename)
        {
            this.mBmp = new Bitmap(filename);
        }

        public void SetBitmap(Bitmap bmp)
        {
            this.mBmp = bmp;
        }

        /// <summary>
        /// Set the PictureBox and add necessary handlers to it
        /// </summary>
        public void SetPictureBox(PictureBox p)
        {
            this.mPictureBox = p;
            mPictureBox.MouseDown += new MouseEventHandler(mPictureBox_MouseDown);
            mPictureBox.MouseUp += new MouseEventHandler(mPictureBox_MouseUp);
            mPictureBox.MouseMove += new MouseEventHandler(mPictureBox_MouseMove);
            mPictureBox.Paint += new PaintEventHandler(mPictureBox_Paint);
        }

        /// <summary>
        /// Set the CheckBox to determine if user is in Select Mode or not and add handlers to it.
        /// </summary>
        public void SetSelectModeCheckBox(CheckBox c)
        {
            this.SelectModeCheck = c;
            selectMode = c.Checked;
            SelectModeCheck.CheckedChanged += new EventHandler(mCheckBox_CheckedChanged);
        }

        public void SetPaint(bool paint)
        {
            this.paint = paint;
            mPictureBox.Invalidate();
        }

        /// <summary>
        /// Called when the value of the checkbox changes
        /// </summary>
        private void mCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //Update the select mode
            selectMode = this.SelectModeCheck.Checked;
            if (!selectMode)
            {
                //If we have left select mode shapes can no longer be selected
                selected = false;
                //Invalidate the picturebox to redraw the surface
                mPictureBox.Invalidate();
            }
        }

        /// <summary>
        /// Called when surface needs to be drawn again
        /// </summary>
        private void mPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (paint)
            {
                try
                {
                    //Draw the shape and if it has been selected also the resize rectangles
                    Draw(e.Graphics);
                    if (selected)
                    {
                        drawResizer(e.Graphics);
                    }
                }
                catch (Exception exp)
                {
                    System.Console.WriteLine(exp.Message);
                }
            }
        }

        /// <summary>
        /// Called when mousedown event happens
        /// </summary>
        private void mPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            mIsClick = true;
            nodeSelected = PosSizableRect.None;
            nodeSelected = GetNodeSelectable(e.Location);

            ////Check if user clicked this rectangle
            if (shape.Contains(new Point(e.X, e.Y)))
            {
                mMove = true;
                if (selectMode && e.Button == MouseButtons.Left)
                {
                    //If we are in selectMode and left mouse button has been click, select this shape
                    selected = true;
                }
                else if (selectMode && e.Button == MouseButtons.Right)
                {
                    //If we are in selectMode and right mouse button has been clicked, unselect this shape
                    selected = false;
                }
                //Invalidate the picturebox to redraw the surface
                mPictureBox.Invalidate();
            }
            oldX = e.X;
            oldY = e.Y;
        }

        /// <summary>
        /// Called when mouseup event triggers
        /// </summary>
        private void mPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            mIsClick = false;
            mMove = false;
        }

        /// <summary>
        /// Called when mousemove event triggers
        /// </summary>
        public virtual void mPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            ChangeCursor(e.Location);
            //If the user is not clicking, return from this function
            if (mIsClick == false)
            {
                return;
            }

            //If this shape is selected
            if (selected)
            {

                Rectangle backupRect = shape;
                //Check which node (resize rectangle) the user has selected
                //And resize the shape based on that
                switch (nodeSelected)
                {
                    case PosSizableRect.LeftUp:
                        shape.X += e.X - oldX;
                        shape.Width -= e.X - oldX;
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        break;
                    case PosSizableRect.LeftMiddle:
                        shape.X += e.X - oldX;
                        shape.Width -= e.X - oldX;
                        break;
                    case PosSizableRect.LeftBottom:
                        shape.Width -= e.X - oldX;
                        shape.X += e.X - oldX;
                        shape.Height += e.Y - oldY;
                        break;
                    case PosSizableRect.BottomMiddle:
                        shape.Height += e.Y - oldY;
                        break;
                    case PosSizableRect.RightUp:
                        shape.Width += e.X - oldX;
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        break;
                    case PosSizableRect.RightBottom:
                        shape.Width += e.X - oldX;
                        shape.Height += e.Y - oldY;
                        break;
                    case PosSizableRect.RightMiddle:
                        shape.Width += e.X - oldX;
                        break;

                    case PosSizableRect.UpMiddle:
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        break;

                    default:
                        if (mMove)
                        {
                            //If the mouse is moving and has not got a node selected, move the shape
                            shape.X = shape.X + e.X - oldX;
                            shape.Y = shape.Y + e.Y - oldY;
                        }
                        break;
                }
                oldX = e.X;
                oldY = e.Y;

                if (shape.Width < 5 || shape.Height < 5)
                {
                    shape = backupRect;
                }

                //Check if rectangle is still inside the area and redraw the surface by calling Invalidate
                TestIfRectInsideArea();
                mPictureBox.Invalidate();
            }
        }

        /// <summary>
        /// // Test if rectangle still inside the area.
        /// </summary>
        public void TestIfRectInsideArea()
        {        
            if (shape.X < 0) shape.X = 0;
            if (shape.Y < 0) shape.Y = 0;
            if (shape.Width <= 0) shape.Width = 1;
            if (shape.Height <= 0) shape.Height = 1;

            if (shape.X + shape.Width > mPictureBox.Width)
            {
                shape.Width = mPictureBox.Width - shape.X - 1;              
            }
            if (shape.Y + shape.Height > mPictureBox.Height)
            {
                shape.Height = mPictureBox.Height - shape.Y - 1;
            }
        }

        /// <summary>
        /// Create a resize rectangle
        /// </summary>
        private Rectangle CreateRectSizableNode(int x, int y)
        {
            return new Rectangle(x - sizeNodeRect / 2, y - sizeNodeRect / 2, sizeNodeRect, sizeNodeRect);
        }

        private Rectangle GetRect(PosSizableRect p)
        {
            switch (p)
            {
                case PosSizableRect.LeftUp:
                    return CreateRectSizableNode(shape.X, shape.Y);

                case PosSizableRect.LeftMiddle:
                    return CreateRectSizableNode(shape.X, shape.Y + +shape.Height / 2);

                case PosSizableRect.LeftBottom:
                    return CreateRectSizableNode(shape.X, shape.Y + shape.Height);

                case PosSizableRect.BottomMiddle:
                    return CreateRectSizableNode(shape.X + shape.Width / 2, shape.Y + shape.Height);

                case PosSizableRect.RightUp:
                    return CreateRectSizableNode(shape.X + shape.Width, shape.Y);

                case PosSizableRect.RightBottom:
                    return CreateRectSizableNode(shape.X + shape.Width, shape.Y + shape.Height);

                case PosSizableRect.RightMiddle:
                    return CreateRectSizableNode(shape.X + shape.Width, shape.Y + shape.Height / 2);

                case PosSizableRect.UpMiddle:
                    return CreateRectSizableNode(shape.X + shape.Width / 2, shape.Y);
                default:
                    return new Rectangle();
            }
        }

        private PosSizableRect GetNodeSelectable(Point p)
        {
            foreach (PosSizableRect r in Enum.GetValues(typeof(PosSizableRect)))
            {
                if (GetRect(r).Contains(p))
                {
                    return r;
                }
            }
            return PosSizableRect.None;
        }

        public void ChangeCursor(Point p)
        {
            mPictureBox.Cursor = GetCursor(GetNodeSelectable(p));
        }

        public void SetSelected(bool selected)
        {
            this.selected = selected;
            mPictureBox.Invalidate();
        }

        /// <summary>
        /// Get cursor for the handle
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private Cursor GetCursor(PosSizableRect p)
        {
            switch (p)
            {
                case PosSizableRect.LeftUp:
                    return Cursors.SizeNWSE;

                case PosSizableRect.LeftMiddle:
                    return Cursors.SizeWE;

                case PosSizableRect.LeftBottom:
                    return Cursors.SizeNESW;

                case PosSizableRect.BottomMiddle:
                    return Cursors.SizeNS;

                case PosSizableRect.RightUp:
                    return Cursors.SizeNESW;

                case PosSizableRect.RightBottom:
                    return Cursors.SizeNWSE;

                case PosSizableRect.RightMiddle:
                    return Cursors.SizeWE;

                case PosSizableRect.UpMiddle:
                    return Cursors.SizeNS;
                default:
                    return Cursors.Default;
            }
        }
        public virtual void Execute()
        {
            
        }

        public virtual void Undo()
        {
            
        }
    }
}

