﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DePDraw
{
    class UserGroup : UserShape
    {
        private List<UserShape> groupShapes = new List<UserShape>();
        Rectangle parent;
        private int width;
        private int height;

        public UserGroup(List<UserShape> groupShapes)
        {
            this.groupShapes = groupShapes;
            parent = new Rectangle();
            foreach (UserShape uShape in groupShapes)
            {
                width += uShape.shape.Width;
                height += uShape.shape.Height;
            }
            width += 10;
            height += 10;
            parent = new Rectangle(0, 0, width, height);
            shape = parent;
        }

        public override void Draw(Graphics g)
        {             
            g.DrawRectangle(new Pen(Color.Red), shape);
            foreach(UserShape myShape in groupShapes)
            {
                if (myShape.GetType() == typeof(UserRect))
                {
                    System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
                    g.DrawRectangle(new Pen(Color.Red), myShape.shape);
                    g.FillRectangle(myBrush, myShape.shape);
                }
                if (myShape.GetType() == typeof(UserEllipse))
                {
                    System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Purple);
                    g.DrawEllipse(new Pen(Color.Red), myShape.shape);
                    g.FillEllipse(myBrush, myShape.shape);
                }
            }
            mPictureBox.Invalidate();
            width = 0;
            height = 0;
        }

        public override void Execute()
        {
            foreach (UserShape uShape in groupShapes)
            {
                uShape.SetPictureBox(this.mPictureBox);
                uShape.SetSelectModeCheckBox(this.SelectModeCheck);
            }
            mPictureBox.Invalidate();
        }

        public override void Undo()
        {
            paint = false;
            mPictureBox.Invalidate();
        }

        /// <summary>
        /// Called when mousemove event triggers
        /// </summary>
        public override void mPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            ChangeCursor(e.Location);
            //If the user is not clicking, return from this function
            if (mIsClick == false)
            {
                return;
            }

            //If this shape is selected
            if (selected)
            {

                Rectangle backupRect = shape;
                //Check which node (resize rectangle) the user has selected
                //And resize the shape based on that
                switch (nodeSelected)
                {
                    case PosSizableRect.LeftUp:
                        shape.X += e.X - oldX;
                        shape.Width -= e.X - oldX;
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.X += e.X - oldX;
                        //    myShape.shape.Width -= e.X - oldX;
                        //    myShape.shape.Y += e.Y - oldY;
                        //    myShape.shape.Height -= e.Y - oldY;
                        //}
                        break;
                    case PosSizableRect.LeftMiddle:
                        shape.X += e.X - oldX;
                        shape.Width -= e.X - oldX;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.X += e.X - oldX;
                        //    myShape.shape.Width -= e.X - oldX;
                        //}
                        break;
                    case PosSizableRect.LeftBottom:
                        shape.Width -= e.X - oldX;
                        shape.X += e.X - oldX;
                        shape.Height += e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Width -= e.X - oldX;
                        //    myShape.shape.X += e.X - oldX;
                        //    myShape.shape.Height += e.Y - oldY;
                        //}
                        break;
                    case PosSizableRect.BottomMiddle:
                        shape.Height += e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Height += e.Y - oldY;
                        //}
                        break;
                    case PosSizableRect.RightUp:
                        shape.Width += e.X - oldX;
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Width += e.X - oldX;
                        //    myShape.shape.Y += e.Y - oldY;
                        //    myShape.shape.Height -= e.Y - oldY;
                        //}
                        break;
                    case PosSizableRect.RightBottom:
                        shape.Width += e.X - oldX;
                        shape.Height += e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Width += e.X - oldX;
                        //    myShape.shape.Height += e.Y - oldY;
                        //}
                        break;
                    case PosSizableRect.RightMiddle:
                        shape.Width += e.X - oldX;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Width += e.X - oldX;
                        //}
                        break;

                    case PosSizableRect.UpMiddle:
                        shape.Y += e.Y - oldY;
                        shape.Height -= e.Y - oldY;
                        //foreach (UserShape myShape in groupShapes)
                        //{
                        //    myShape.shape.Y += e.Y - oldY;
                        //    myShape.shape.Height -= e.Y - oldY;
                        //}
                        break;

                    default:
                        if (mMove)
                        {
                            //If the mouse is moving and has not got a node selected, move the shape
                            shape.X = shape.X + e.X - oldX;
                            shape.Y = shape.Y + e.Y - oldY;
                            foreach (UserShape myShape in groupShapes)
                            {
                                myShape.shape.X = myShape.shape.X + e.X - oldX;
                                myShape.shape.Y = myShape.shape.Y + e.Y - oldY;
                            }
                        }
                        break;
                }
                oldX = e.X;
                oldY = e.Y;

                if (shape.Width < 5 || shape.Height < 5)
                {
                    shape = backupRect;
                }

                //Check if rectangle is still inside the area and redraw the surface by calling Invalidate
                TestIfRectInsideArea();
                foreach (UserShape myShape in groupShapes)
                {
                    myShape.TestIfRectInsideArea();
                    myShape.TestIfRectInsideArea();
                }
                mPictureBox.Invalidate();
            }
        }

 

    }
}
