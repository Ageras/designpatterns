﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DePDraw
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            FileReader fileReader = new FileReader(@"E:\NHL Hogeschool\DeP\Git\IOTest.txt", this.checkBox1, this.pictureBox1);
        }

        /// <summary>
        /// When button gets clicked create a new rectangle and set its values.
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Drawer.DrawRectangle(10, 10, 100, 100, this.pictureBox1, this.checkBox1);
            //Drawer.DrawGroup(10, 10, 100, 100, this.pictureBox1, this.checkBox1);
        }


        /// <summary>
        /// When button gets clicked create a new ellipse and set its values.
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            Drawer.DrawEllipse(10, 10, 100, 100, this.pictureBox1, this.checkBox1);       
        }

        /// <summary>
        /// When button gets clicked, undo the latest action
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            Drawer.Undo();
        }

    }
}
