﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace DePDraw
{
    static class Drawer
    {
        private static Stack<Command> history = new Stack<Command>();

        public static void DrawRectangle(int x, int y, int w, int h, PictureBox pictureBox, CheckBox checkBox)
        {
            //Create a new command to add a rectangle
            UserShape newCommand = new UserRect(new Rectangle(x, y, w, h));
            newCommand.SetPictureBox(pictureBox);
            newCommand.SetSelectModeCheckBox(checkBox);
            //Execute the command to draw the rectangle and add it to the history
            newCommand.Execute();
            history.Push(newCommand);
        }

        public static void DrawEllipse(int x, int y, int w, int h, PictureBox pictureBox, CheckBox checkBox)
        {
            //Create a new command to add a ellipse
            UserShape newCommand = new UserEllipse(new Rectangle(x, y, w, h));
            newCommand.SetPictureBox(pictureBox);
            newCommand.SetSelectModeCheckBox(checkBox);
            //Execute the command to draw the ellipse and add it to the history
            newCommand.Execute();
            history.Push(newCommand);
        }

        public static void DrawGroup(List<UserShape> shapeList, PictureBox pictureBox, CheckBox checkBox)
        {
            //Create a new command to add a group
            UserShape newCommand = new UserGroup(shapeList);
            newCommand.SetPictureBox(pictureBox);
            newCommand.SetSelectModeCheckBox(checkBox);
            //Execute the command to draw the group and add it to the history
            newCommand.Execute();
            history.Push(newCommand);
        }

        public static void Undo()
        {
            //If there is something left in the history, undo it
            if (history.Count > 0)
            {
                history.Pop().Undo();
            }
        }
    }
}
