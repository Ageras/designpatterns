﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DePDraw
{
    class UserRect : UserShape
    {
        public UserRect(Rectangle r)
        {
           shape = r;
           mIsClick = false;
        }

        public override void Draw(Graphics g)
        {
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
            g.DrawRectangle(new Pen(Color.Red), shape);
            g.FillRectangle(myBrush, shape);
        }

        public override void Execute()
        {
            mPictureBox.Invalidate();
        }

        public override void Undo()
        {
            paint = false;
            mPictureBox.Invalidate();
        }
    }
}

