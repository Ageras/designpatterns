﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DePDraw
{
    class FileReader
    {
        private CheckBox checkBox;
        private PictureBox pictureBox;

        public FileReader(string path, CheckBox checkBox, PictureBox pictureBox)
        {
            this.checkBox = checkBox;
            this.pictureBox = pictureBox;

            //Split all the files lines
            //string[] lines = System.IO.File.ReadAllLines(path);
            string[] lines = new string[3];
            lines[0] = "group 2";
            lines[1] = " ellipse 100 100 20 50";
            lines[2] = " rectangle 10 20 100 100";
            for (int i = 0; i < lines.Length; i++ )
            {
                //Split the line based on ' ' and try to find rectangle or ellipse
                string[] splitLines = lines[i].Split(' ');
                bool rect = false;
                bool ellipse = false;
                bool group = false;
                int indexOfShape = 0;
                foreach(string line in splitLines)
                {
                    if(line == "rectangle")
                    {
                        rect = true;
                        break;
                    }
                    if(line == "ellipse")
                    {
                        ellipse = true;
                        break;
                    }
                    if(line == "group")
                    {
                        group = true;
                        break;
                    }
                    indexOfShape++;
                }
                //If rectangle has been found draw the rectangle
                if(rect)
                {
                    Drawer.DrawRectangle(Int32.Parse(splitLines[indexOfShape + 1]), Int32.Parse(splitLines[indexOfShape + 2]), 
                                                     Int32.Parse(splitLines[indexOfShape + 3]), Int32.Parse(splitLines[indexOfShape + 4]), 
                                                     this.pictureBox, this.checkBox);            
                }
                //Else if ellipse has been found draw the ellipse
                else if(ellipse)
                {
                    Drawer.DrawEllipse(Int32.Parse(splitLines[indexOfShape + 1]), Int32.Parse(splitLines[indexOfShape + 2]),
                                 Int32.Parse(splitLines[indexOfShape + 3]), Int32.Parse(splitLines[indexOfShape + 4]),
                                 this.pictureBox, this.checkBox);    
                }
                else if(group)
                {
                    indexOfShape++;
                    List<UserShape> shapeList = new List<UserShape>();
                    int groupSize = Int32.Parse(splitLines[indexOfShape]);
                    for (int j = 1; j <= groupSize; j++ )
                    {
                        bool isRect = false;
                        bool isEllipse = false;
                        UserShape newShape;
                        string[] splitShape = lines[i + j].Split(' ');

                        foreach(string line in splitShape)
                        {
                            if(line == "rectangle")
                            {
                                isRect = true;
                                break;
                            }
                            if (line == "ellipse")
                            {
                                isEllipse = true;
                                break;
                            }
                        }

                        if(isRect)
                        {
                            Rectangle newShapeRect = new Rectangle(Int32.Parse(splitShape[indexOfShape + 1]), Int32.Parse(splitShape[indexOfShape + 2]),
                                 Int32.Parse(splitShape[indexOfShape + 3]), Int32.Parse(splitShape[indexOfShape + 4]));
                            newShape = new UserRect(newShapeRect);
                            shapeList.Add(newShape);
                        }
                        if(isEllipse)
                        {
                            Rectangle newShapeRect = new Rectangle(Int32.Parse(splitShape[indexOfShape + 1]), Int32.Parse(splitShape[indexOfShape + 2]),
                                 Int32.Parse(splitShape[indexOfShape + 3]), Int32.Parse(splitShape[indexOfShape + 4]));
                            newShape = new UserEllipse(newShapeRect);
                            shapeList.Add(newShape);
                        }
                    }
                    Console.WriteLine(groupSize);
                    i += groupSize;
                    Drawer.DrawGroup(shapeList, this.pictureBox, this.checkBox);
                }
            }           
        }
    }
}
