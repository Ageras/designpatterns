﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DePDraw
{
    interface Command
    {
        void Execute();
        void Undo();
    }
}
